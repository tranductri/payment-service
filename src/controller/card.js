const { checkInfo } = require('../action/card')

const getInfoCard = async (req, res) => {
    res.status(200).send(checkInfo())
}

module.exports = {
    getInfoCard
}
