const express = require('express')
const router = express.Router()

const { getInfoCard } = require('../controller/card')

router.route('/info').get(getInfoCard)

module.exports = router
