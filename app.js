const express = require('express')
const dotenv = require('dotenv')
const cardRouter = require('./src/router/card')
const { middleware } = require('libs')

dotenv.config()
const app = express()
const port = process.env.PORT || 3001

app.use(express.json());


app.use('/api/v1/cards', cardRouter)

app.use(middleware.notFound)
app.use(middleware.errorHander)

app.get('/', (req, res) => {
    res.send('Hello Im Payment service')
})

app.listen(port, () => {
    console.log(`[server]: Server is running at port: ${ port }`)
})
